import React, { Component } from "react";
import Home from "./Containers/Home";
import DetailNews from './Components/News/DetailNews'
import Navbar from "./Components/Navbar";

import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { Provider } from "react-redux";
import store from './store'

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Router>
          <>
            <Navbar />            
              <Switch>
                <Route exact path="/" component={Home} />      
                <Route exact path="/news/:id" component={DetailNews} />                   
              </Switch>            
          </>
        </Router>
      </Provider>
    );
  }
}

export default App;
