import React from 'react'

export default () => {
  return (
    <nav className="navbar navbar-default">
		<div className="container-fluid first-nav">
			<div className="navbar-left">
				<img src="/assets/img/logo.png" className="logo"/>
				<div className="menu-micon">
					<div id="toggle">
						<i className="fa fa-bars"></i>
					</div>
				</div>
			</div>
			<div className="navbar-right rate">
				<div className="rate-group">
					<div className="rate-item">
						{/* <div className="rate-title">BTC</div>
						<div className="rate-price" id="btc-price">$ -</div>
						<div className="rate-hr2"></div>
						<div className="rate-percent" id="btc-percent"></div> */}
					</div>
					<div className="rate-item rate-desktop">
						<div className="rate-title">CFIxCFI</div>
						<div className="rate-price" id="eth-price">$ -</div>
						<div className="rate-hr2"></div>
						<div className="rate-percent" id="eth-percent"></div>
					</div>
					<div className="rate-item rate-desktop">
						{/* <div className="rate-title">CFIx</div>
						<div className="rate-price" id="xrp-price">$ -</div>
						<div className="rate-hr2"></div>
						<div className="rate-percent" id="xrp-percent"></div> */}
					</div>
					<div className="rate-item rate-desktop">
						{/* <div className="rate-title">BCH</div>
						<div className="rate-price" id="bch-price">$ -</div>
						<div className="rate-hr2"></div>
						<div className="rate-percent" id="bch-percent"></div> */}
					</div>
					<div className="rate-item rate-desktop">
						{/* <div className="rate-title">LTC</div>
						<div className="rate-price" id="ltc-price">$ -</div>
						<div className="rate-hr2"></div>
						<div className="rate-percent" id="ltc-percent"></div> */}
					</div>
				</div>
			</div>
		</div>
		<div className="container-fluid" id="menu">
			<ul className="nav navbar-nav" id="mobile">
				<form className="navbar-form navbar-right" action="">
					<div className="form-group search-group">
						<span className="fa fa-search"></span>
						<input type="text" className="form-control search-bar" placeholder="Search Here"/>
						<input type="submit" value="Search" className="btn-search"/>
					</div>
				</form>
				<li><a href="#section-four" className="mobile-sub">NEWSLETTER</a></li>
				<li><a href="#">HOME</a></li>
				<li><a href="#">ABOUT</a></li>
				<li><a href="#">NEWS</a></li>
				<li><a href="#">EVENTS</a></li>
				<li><a href="#">BUSINESS</a></li>
				<li><a href="#">MARKETS</a></li>
				<li><a href="#">TECHNOLOGY</a></li>
				<li><a href="#">BLOCKCHAIN</a></li>
			</ul>
			<ul className="nav navbar-nav" id="desktop">
				<li><a href="#" >HOME</a></li>
				<li><a href="#" >ABOUT</a></li>
				<li><a href="#" >NEWS</a></li>
				<li><a href="#" >EVENTS</a></li>
				<li><a href="#" >BUSINESS</a></li>
				<li><a href="#" >MARKETS</a></li>
				<li><a href="#" >TECHNOLOGY</a></li>
				<li><a href="#" >BLOCKCHAIN</a></li>
			</ul>
			<form className="navbar-form navbar-right" action="" id="desktop-search">
				<div className="form-group search-group">
					<span className="fa fa-search"></span>
					<input type="text" className="form-control search-bar" placeholder="Search Here"/>
				</div>
			</form>
		</div>
	</nav>
  )
}
