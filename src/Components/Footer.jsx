import React from "react";

const Footer = () => {
  return (
    <div id="section-four">
      <div className="subscribe">
        <div className="subscribe-title">
          Subscribe to the High Five Newsletter!!
        </div>
        <div className="subscribe-input">
          <input
            type="email"
            name="subscribe-email"
            placeholder="Your email here"
          />
          <button>
            <i className="fa fa-paper-plane" />
          </button>
        </div>
      </div>
      <div className="footer">
        <div className="footer-logogroup">
          <img src="assets/img/logo.png" className="footer-logo" />
        </div>
        <div className="footer-menu">
          <a href="">Home</a>
          <a href="">About</a>
          <a href="">News</a>
          <a href="">Events</a>
          <a href="">Business</a>
          <a href="">Markets</a>
          <a href="">Technology</a>
          <a href="">Blockchain</a>
        </div>
        <div className="footer-term">
          <a href="">Term of Service and Privacy Policy</a>
        </div>
      </div>
      <div className="copyright">
        <div>Copyright {'&copy2018'} Crypto Flow News. All Rights Reserved.</div>
      </div>
    </div>
  );
};

export default Footer;
