import React, { Component } from "react";
import firestore from "../../firestore";
import { Link } from "react-router-dom";

const News = props => {
  const {
    title,
    keywords_nltk,
    summary_nltk,
    image_name,
    author,
    upload_date,
    link
  } = props.data
  let date = {
    seconds: upload_date.seconds,
    nanoseconds: upload_date.nanoseconds
  }

  const newDate = new Date(date)

  const az = new Date(upload_date * 1000).toString() 

  return (
    <div className="article-item">
      <div
        className="article-img"
        style={{ backgroundImage: `url(${image_name})` }}
      >
        <div className="taggroup">
          <div className="btn btn-blockchain tag">BLOCKCHAINs</div>
        </div>
      </div>
      <div className="article-group">
        <a href={link}>
          <div className="article-title">{title}</div>
        </a>

        <div className="article-detail">
          <div className="article-date">
            <i className="fa fa-clock-o" />
            {az}
          </div>
          <div className="article-author">
            <i className="fa fa-user" />
            by {author.length > 0 ? author.join(", ") : "unknown"}
          </div>
        </div>
        <div className="article-content">{summary_nltk}</div>
        <div className="article-link">
          <div className="article-linkdet">
            <a href="">Keywords:</a>
            <div>{keywords_nltk.join(", ")}</div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default News;
