import React, { Component } from "react";
import News from "./News";
import { compose } from "redux";
import { connect } from "react-redux";
import { firestoreConnect } from "react-redux-firebase";
import LoadMore from "../UI/LoadMore";

class NewsWrapper extends Component {

  state = {
    // articles: [{title: {upload_date: }}]
  }
  render() {
    const { articles } = this.props;
    return (
      <>
        <div id="section-two">
          {articles ? (
            articles.map(article => <News key={article.id}  data={article} />)
          ) : (
            <h1>Loading ...</h1>
          )}
        </div>
        <LoadMore onClick={() => console.log('aa')} />
      </>
    );
  }
}

const mapStateToProps = state => {
  return {
    articles: state.firestore.ordered.articles
  };
};

export default compose(
  firestoreConnect([
    {
      collection: "CFN_article",
      storeAs: "articles",
      where: ["top_news", "==", false]
    }
  ]),
  connect(mapStateToProps)
)(NewsWrapper);
