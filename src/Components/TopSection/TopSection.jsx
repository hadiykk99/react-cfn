import React, { Component } from 'react'

export default class TopSection extends Component {
  render() {
    return (
      <div id="section-one">
      <div className="tile-l" id="new">
        <div className="slidepage">
          <div id="prev2">
            <img src="assets/img/left.png" />
          </div>
          <div id="next2">
            <img src="assets/img/right.png" />
          </div>
        </div>
        <div
          className="tile-slide sactive"
          id="S1"
          slide-index="1"
          style={{backgroundImage: "url('assets/img/slide1.jpg')"}}
        >
          <div className="btn btn-topnews">TOP NEWS</div>
          <div className="tile-taggroup">
            <div className="btn btn-markets tile-tag">MARKETS</div>
          </div>
          <div className="tile-title">
            <a href="">
              Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies
              nisi. Nam eget dui. tempus, tellus eget condimentum{" "}
            </a>
          </div>
          <div className="tile-detail">
            <div className="tile-date">
              <i className="fa fa-clock-o" />
              22 Dec 2017
            </div>
            <div className="article-author">
              <i className="fa fa-user" />
              by John Alexa
            </div>
            <div className="tile-comment">
              <i className="fa fa-comments-o" />
              2,714
            </div>
            <div className="tile-view">
              <i className="fa fa-eye" />
              28,741
            </div>
          </div>
        </div>
        <div
          className="tile-slide"
          id="S2"
          slide-index="2"
          style={{backgroundImage: "url('assets/img/slide1.jpg')"}}
        >
          <div className="btn btn-topnews">TOP NEWS</div>
          <div className="tile-taggroup">
            <div className="btn btn-technology tile-tag">TECHNOLOGY</div>
          </div>
          <div className="tile-title">
            <a href="">Lala</a>
          </div>
          <div className="tile-detail">
            <div className="tile-date">
              <i className="fa fa-clock-o" />
              22 Dec 2017
            </div>
            <div className="article-author">
              <i className="fa fa-user" />
              by John Alexa
            </div>
            <div className="tile-comment">
              <i className="fa fa-comments-o" />
              2,714
            </div>
            <div className="tile-view">
              <i className="fa fa-eye" />
              28,741
            </div>
          </div>
        </div>
        <div
          className="tile-slide"
          id="S3"
          slide-index="3"
          style={{backgroundImage: "url('assets/img/slide1.jpg')"}}
        >
          <div className="btn btn-topnews">TOP NEWS</div>
          <div className="tile-taggroup">
            <div className="btn btn-technology tile-tag">TECHNOLOGY</div>
          </div>
          <div className="tile-title">
            <a href="">WKWKWKWK</a>
          </div>
          <div className="tile-detail">
            <div className="tile-date">
              <i className="fa fa-clock-o" />
              22 Dec 2017
            </div>
            <div className="article-author">
              <i className="fa fa-user" />
              by John Alexa
            </div>
            <div className="tile-comment">
              <i className="fa fa-comments-o" />
              2,714
            </div>
            <div className="tile-view">
              <i className="fa fa-eye" />
              28,741
            </div>
          </div>
        </div>
      </div>

      <div className="tile-group">
        <div className="tile-l" id="desktop" style={{order:2}}>
          <div className="slidepage">
            <div id="prev">
              <img src="assets/img/left.png" />
            </div>
            <div id="next">
              <img src="assets/img/right.png" />
            </div>
          </div>
          <div
            className="tile-slide active"
            id="1"
            slide-index="1"
            style={{backgroundImage: "url('assets/img/slide1.jpg')"}}
          >
            <div className="btn btn-topnews">TOP NEWS</div>
            <div className="tile-taggroup">
              <div className="btn btn-markets tile-tag">MARKETS</div>
            </div>
            <div className="tile-title">
              <a href="">
                Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies
                nisi. Nam eget dui. tempus, tellus eget condimentum{" "}
              </a>
            </div>
            <div className="tile-detail">
              <div className="tile-date">
                <i className="fa fa-clock-o" />
                22 Dec 2017
              </div>
              <div className="article-author">
                <i className="fa fa-user" />
                by John Alexa
              </div>
              <div className="tile-comment">
                <i className="fa fa-comments-o" />
                2,714
              </div>
              <div className="tile-view">
                <i className="fa fa-eye" />
                28,741
              </div>
            </div>
          </div>
          <div
            className="tile-slide"
            id="2"
            slide-index="2"
            style={{backgroundImage: "url('assets/img/slide1.jpg')"}}
          >
            <div className="btn btn-topnews">TOP NEWS</div>
            <div className="tile-taggroup">
              <div className="btn btn-technology tile-tag">TECHNOLOGY</div>
            </div>
            <div className="tile-title">
              <a href="">Lala</a>
            </div>
            <div className="tile-detail">
              <div className="tile-date">
                <i className="fa fa-clock-o" />
                22 Dec 2017
              </div>
              <div className="article-author">
                <i className="fa fa-user" />
                by John Alexa
              </div>
              <div className="tile-comment">
                <i className="fa fa-comments-o" />
                2,714
              </div>
              <div className="tile-view">
                <i className="fa fa-eye" />
                28,741
              </div>
            </div>
          </div>
          <div
            className="tile-slide"
            id="3"
            slide-index="3"
            style={{backgroundImage: "url('assets/img/slide1.jpg')"}}
          >
            <div className="btn btn-topnews">TOP NEWS</div>
            <div className="tile-taggroup">
              <div className="btn btn-technology tile-tag">TECHNOLOGY</div>
            </div>
            <div className="tile-title">
              <a href="">WKWKWKWK</a>
            </div>
            <div className="tile-detail">
              <div className="tile-date">
                <i className="fa fa-clock-o" />
                22 Dec 2017
              </div>
              <div className="article-author">
                <i className="fa fa-user" />
                by John Alexa
              </div>
              <div className="tile-comment">
                <i className="fa fa-comments-o" />
                2,714
              </div>
              <div className="tile-view">
                <i className="fa fa-eye" />
                28,741
              </div>
            </div>
          </div>
        </div>

        <div className="tile-m tile-mobile" style={{order:1}}>
          <div
            className="tile-s"
            style={{backgroundImage: "url('assets/img/slide1.jpg')"}}
          >
            <div className="tile-taggroup">
              <div className="btn btn-events tile-tag">EVENTS</div>
            </div>
            <div className="tile-title">
              Nullam dictum felis eu pede mollis pretium.
            </div>
            <div className="tile-detail">
              <div className="tile-date">
                <i className="fa fa-clock-o" />
                22 Dec 2017
              </div>
              <div className="tile-comment">
                <i className="fa fa-comments-o" />
                2,714
              </div>
              <div className="tile-view">
                <i className="fa fa-eye" />
                28,741
              </div>
            </div>
            <div className="tile-hover">
              <div className="tile-title">
                Nullam dictum felis eu pede mollis pretium.
              </div>
              <div className="tile-detail">
                <div className="tile-date">
                  <i className="fa fa-clock-o" />
                  22 Dec 2017
                </div>
                <div className="tile-comment">
                  <i className="fa fa-comments-o" />
                  2,714
                </div>
                <div className="tile-view">
                  <i className="fa fa-eye" />
                  28,741
                </div>
              </div>
              <div>
                Lorem ipsum
                <a href="">Read more..</a>
              </div>
            </div>
          </div>
          <div
            className="tile-s"
            style={{backgroundImage: "url('assets/img/slide1.jpg')"}}
          >
            <div className="tile-taggroup">
              <div className="btn btn-business tile-tag">BUSINESS</div>
            </div>
            <div className="tile-title">
              Nullam dictum felis eu pede mollis pretium.
            </div>
            <div className="tile-detail">
              <div className="tile-date">
                <i className="fa fa-clock-o" />
                22 Dec 2017
              </div>
              <div className="tile-comment">
                <i className="fa fa-comments-o" />
                2,714
              </div>
              <div className="tile-view">
                <i className="fa fa-eye" />
                28,741
              </div>
            </div>
            <div className="tile-hover">
              <div className="tile-title">
                Nullam dictum felis eu pede mollis pretium.
              </div>
              <div className="tile-detail">
                <div className="tile-date">
                  <i className="fa fa-clock-o" />
                  22 Dec 2017
                </div>
                <div className="tile-comment">
                  <i className="fa fa-comments-o" />
                  2,714
                </div>
                <div className="tile-view">
                  <i className="fa fa-eye" />
                  28,741
                </div>
              </div>
              <div>
                Lorem ipsum
                <a href="">Read more..</a>
              </div>
            </div>
          </div>
        </div>

        <div className="tile-m tile-desktop" style={{order:3}}>
          <div
            className="tile-s"
            style={{backgroundImage: "url('assets/img/slide1.jpg')"}}
          >
            <div className="tile-taggroup">
              <div className="btn btn-news tile-tag">NEWS</div>
            </div>
            <div className="tile-title">
              Nullam dictum felis eu pede mollis pretium.
            </div>
            <div className="tile-detail">
              <div className="tile-date">
                <i className="fa fa-clock-o" />
                22 Dec 2017
              </div>
              <div className="tile-comment">
                <i className="fa fa-comments-o" />
                2,714
              </div>
              <div className="tile-view">
                <i className="fa fa-eye" />
                28,741
              </div>
            </div>
            <div className="tile-hover">
              <div className="tile-title">
                Nullam dictum felis eu pede mollis pretium.
              </div>
              <div className="tile-detail">
                <div className="tile-date">
                  <i className="fa fa-clock-o" />
                  22 Dec 2017
                </div>
                <div className="tile-comment">
                  <i className="fa fa-comments-o" />
                  2,714
                </div>
                <div className="tile-view">
                  <i className="fa fa-eye" />
                  28,741
                </div>
              </div>
              <div>
                Lorem ipsum
                <a href="">Read more..</a>
              </div>
            </div>
          </div>
          <div
            className="tile-s"
            style={{backgroundImage: "url('assets/img/slide1.jpg')"}}
          >
            <div className="tile-taggroup">
              <div className="btn btn-blockchain tile-tag">BLOCKCHAIN</div>
            </div>
            <div className="tile-title">
              Nullam dictum felis eu pede mollis pretium.
            </div>
            <div className="tile-detail">
              <div className="tile-date">
                <i className="fa fa-clock-o" />
                22 Dec 2017
              </div>
              <div className="tile-comment">
                <i className="fa fa-comments-o" />
                2,714
              </div>
              <div className="tile-view">
                <i className="fa fa-eye" />
                28,741
              </div>
            </div>
            <div className="tile-hover">
              <div className="tile-title">
                Nullam dictum felis eu pede mollis pretium.
              </div>
              <div className="tile-detail">
                <div className="tile-date">
                  <i className="fa fa-clock-o" />
                  22 Dec 2017
                </div>
                <div className="tile-comment">
                  <i className="fa fa-comments-o" />
                  2,714
                </div>
                <div className="tile-view">
                  <i className="fa fa-eye" />
                  28,741
                </div>
              </div>
              <div>
                Lorem ipsum
                <a href="">Read more..</a>
              </div>
            </div>
          </div>
        </div>

        <div className="tile-m tile-desktop" style={{order:4}}>
          <div
            className="tile-s"
            style={{backgroundImage: "url('assets/img/slide1.jpg')"}}
          >
            <div className="tile-taggroup">
              <div className="btn btn-business tile-tag">BUSINESS</div>
            </div>
            <div className="tile-title">
              Nullam dictum felis eu pede mollis pretium.
            </div>
            <div className="tile-detail">
              <div className="tile-date">
                <i className="fa fa-clock-o" />
                22 Dec 2017
              </div>
              <div className="tile-comment">
                <i className="fa fa-comments-o" />
                2,714
              </div>
              <div className="tile-view">
                <i className="fa fa-eye" />
                28,741
              </div>
            </div>
            <div className="tile-hover">
              <div className="tile-title">
                Nullam dictum felis eu pede mollis pretium.
              </div>
              <div className="tile-detail">
                <div className="tile-date">
                  <i className="fa fa-clock-o" />
                  22 Dec 2017
                </div>
                <div className="tile-comment">
                  <i className="fa fa-comments-o" />
                  2,714
                </div>
                <div className="tile-view">
                  <i className="fa fa-eye" />
                  28,741
                </div>
              </div>
              <div>
                Lorem ipsum
                <a href="">Read more..</a>
              </div>
            </div>
          </div>
          <div
            className="tile-s"
            id="check"
            style={{backgroundImage: "url('assets/img/slide1.jpg')"}}
          >
            <div className="tile-taggroup">
              <div className="btn btn-technology tile-tag">TECHNOLOGY</div>
            </div>
            <div className="tile-title">
              Nullam dictum felis eu pede mollis pretium.
            </div>
            <div className="tile-detail">
              <div className="tile-date">
                <i className="fa fa-clock-o" />
                22 Dec 2017
              </div>
              <div className="tile-comment">
                <i className="fa fa-comments-o" />
                2,714
              </div>
              <div className="tile-view">
                <i className="fa fa-eye" />
                28,741
              </div>
            </div>
            <div className="tile-hover">
              <div className="tile-title">
                Nullam dictum felis eu pede mollis pretium.
              </div>
              <div className="tile-detail">
                <div className="tile-date">
                  <i className="fa fa-clock-o" />
                  22 Dec 2017
                </div>
                <div className="tile-comment">
                  <i className="fa fa-comments-o" />
                  2,714
                </div>
                <div className="tile-view">
                  <i className="fa fa-eye" />
                  28,741
                </div>
              </div>
              <div>
                Lorem ipsum
                <a href="">Read more..</a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="news-line">
        <div className="news-tag1">BREAKING NEWS</div>
        <div className="news-tag2 btn-black">NEW!!</div>
        <div className="news-datetime">15:50 - 27 Dec 2017</div>
        <div className="news-headline">
          Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus.
          Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper
          libero!!
        </div>
      </div>
    </div>
    )
  }
}
