import React, { Component } from "react";
import TopSection from '../Components/TopSection/TopSection'
// import LoadMore from "../Components/UI/LoadMore";
import Footer from "../Components/Footer";
import NewsWrapper from "../Components/News/NewsWrapper";

export default class Home extends Component {
  render() {
    return (
      <>
      <TopSection />
      <NewsWrapper />
      {/* <LoadMore /> */}
      <Footer />
      </>
    )
  }
}
