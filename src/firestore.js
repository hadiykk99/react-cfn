import firebase from "firebase";
import "firebase/firestore";

const firebaseConfig = {
  apiKey: "AIzaSyCSJF2SYKWGjkNaJFDCSpIcvnIVlnQDICE",
  authDomain: "cfix00001.firebaseapp.com",
  databaseURL: "https://cfix00001.firebaseio.com",
  projectId: "cfix00001",
  storageBucket: "cfix00001.appspot.com",
  messagingSenderId: "422506388715"
};

firebase.initializeApp(firebaseConfig);

const firestore = firebase.firestore();
const settings = {
  timestampsInSnapshots: true
};
firestore.settings(settings);

export default firestore
